# WSScribe

## Setup

* govendor is used to manage dependencies
* Env Vars

  * `GOPATH` should be set to the `go` subdirectory
  * `GOOGLE_APPLICATION_CREDENTIALS` should be the path to the google cloud authentication credentials

* Build the transciption server
  * `go install cyclops/wsscribeserver`
  * This will install `wsscribeserver` to the `$GOPATH/bin` directory
  * The default port it runs on is `8080`, but this can be controlled with the `WSSCRIBE_PORT` env var
* Serve the testing client's assets
  * From the `client` subdirectory run `python -m SimpleHTTPServer 8000` (or really any simple http file server will work)
