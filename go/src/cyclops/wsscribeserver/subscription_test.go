package main

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
	status "google.golang.org/genproto/googleapis/rpc/status"
)

type mockMessage struct {
	msgType int
	msg     []byte
	err     error
}
type mockMessageReader struct {
	messages []mockMessage
	index    int
}

func (m *mockMessageReader) ReadMessage() (int, []byte, error) {
	msg := m.messages[m.index]
	m.index++
	return msg.msgType, msg.msg, msg.err
}

type mockAudioSender struct {
	sent []*speechpb.StreamingRecognizeRequest
	err  error
}

func (m *mockAudioSender) Send(r *speechpb.StreamingRecognizeRequest) error {
	m.sent = append(m.sent, r)
	return m.err
}

func Test_readWS(t *testing.T) {
	t.Run("simple session then go away", func(t *testing.T) {
		mr := &mockMessageReader{
			messages: []mockMessage{
				{websocket.BinaryMessage, []byte(`abc`), nil},
				{websocket.BinaryMessage, []byte(`123`), nil},
				{0, nil, &websocket.CloseError{Code: websocket.CloseGoingAway}},
			},
		}
		as := &mockAudioSender{}
		err := readWS(mr, as)
		if err != nil {
			t.Errorf("unexpected error %+v", err)
		}
		expect := []*speechpb.StreamingRecognizeRequest{
			newAudioRequest(mr.messages[0].msg),
			newAudioRequest(mr.messages[1].msg),
		}
		if !reflect.DeepEqual(expect, as.sent) {
			t.Errorf("unexpected request set: %+v", as.sent)
		}
	})

	t.Run("transcription service error", func(t *testing.T) {
		mr := &mockMessageReader{messages: []mockMessage{{msgType: websocket.BinaryMessage}}}
		as := &mockAudioSender{err: errors.New("AS error")}
		err := readWS(mr, as)
		if err != as.err {
			t.Errorf("expected audio sender error, got: %+v", err)
		}
	})

	badTypes := []struct {
		name string
		code int
	}{{"text msg", websocket.TextMessage}, {"unknown msg", -1}}

	for _, bt := range badTypes {
		t.Run(bt.name, func(t *testing.T) {
			mr := &mockMessageReader{messages: []mockMessage{{msgType: bt.code}}}
			as := &mockAudioSender{}
			err := readWS(mr, as)
			if errors.Cause(err) != errUnexpectedMessageType {
				t.Errorf("unexpected error: %+v", err)
			}
		})
	}

}

type mockAudioReceiver struct {
	responses []*speechpb.StreamingRecognizeResponse
	index     int
	err       error
}

func (m *mockAudioReceiver) Recv() (*speechpb.StreamingRecognizeResponse, error) {
	if m.err != nil {
		return nil, m.err
	}
	r := m.responses[m.index]
	m.index++
	return r, nil
}

func Test_readTranscription(t *testing.T) {
	t.Run("returns the first result from a non-errored, non-empty response", func(t *testing.T) {
		expect := &speechpb.StreamingRecognitionResult{IsFinal: true}
		ar := &mockAudioReceiver{
			responses: []*speechpb.StreamingRecognizeResponse{
				{}, {}, {},
				{Results: []*speechpb.StreamingRecognitionResult{expect}},
			},
		}
		result, err := readTranscription(ar)
		if err != nil {
			t.Errorf("unexpected error: %+v", err)
		}
		if result != expect {
			t.Errorf("expected %+v, got %+v", expect, result)
		}
	})

	t.Run("returns receive errors", func(t *testing.T) {
		ar := &mockAudioReceiver{
			err: errors.New("some error"),
		}
		_, err := readTranscription(ar)
		if errors.Cause(err) != ar.err {
			t.Errorf("expected %+v, got %+v", ar.err, err)
		}
	})

	t.Run("returns RPC status errors", func(t *testing.T) {
		ar := &mockAudioReceiver{
			responses: []*speechpb.StreamingRecognizeResponse{
				{Error: &status.Status{}},
			},
		}
		_, err := readTranscription(ar)
		if errors.Cause(err) != errRPCStatus {
			t.Errorf("expected %+v, got %+v", errRPCStatus, err)
		}
	})
}

type sentMessage struct {
	msgType int
	data    string
}
type mockMessageWriter struct {
	messages []sentMessage
	err      error
}

func (m *mockMessageWriter) WriteMessage(msgType int, payload []byte) error {
	if m.err != nil {
		return m.err
	}
	m.messages = append(m.messages, sentMessage{msgType, string(payload)})
	return nil
}

func Test_write(t *testing.T) {
	t.Run("writes message bytes until send channel is closed", func(t *testing.T) {
		send := make(chan []byte, 512)
		ticker := &time.Ticker{
			C: make(chan time.Time),
		}
		mr := &mockMessageWriter{}
		go write(mr, send, ticker)
		a, b, c := "a", "b", "c"
		send <- []byte(a)
		send <- []byte(b)
		send <- []byte(c)
		close(send)
		time.Sleep(time.Second) // allow for messge processing time

		expect := []sentMessage{
			{websocket.TextMessage, a},
			{websocket.TextMessage, b},
			{websocket.TextMessage, c},
			{websocket.CloseMessage, ""},
		}
		if len(mr.messages) != len(expect) {
			t.Errorf("expected %d messages, got %d", len(expect), len(mr.messages))
		}

		for i, exp := range expect {
			fmt.Println(i)
			if exp != mr.messages[i] {
				t.Errorf("at %d, expected %+v, got %+v", i, exp, mr.messages[i])
			}
		}
	})

	t.Run("pings the socket on a ticker", func(t *testing.T) {
		send := make(chan []byte, 512)
		tickerChan := make(chan time.Time)
		ticker := &time.Ticker{
			C: tickerChan,
		}
		mr := &mockMessageWriter{}
		go write(mr, send, ticker)
		tickerChan <- time.Now()
		tickerChan <- time.Now()
		close(send)
		time.Sleep(time.Second) // allow for messge processing time

		expect := []sentMessage{
			{websocket.PingMessage, ""},
			{websocket.PingMessage, ""},
			{websocket.CloseMessage, ""},
		}
		if len(mr.messages) != len(expect) {
			t.Errorf("expected %d messages, got %d", len(expect), len(mr.messages))
		}

		for i, exp := range expect {
			fmt.Println(i)
			if exp != mr.messages[i] {
				t.Errorf("at %d, expected %+v, got %+v", i, exp, mr.messages[i])
			}
		}

	})

}
