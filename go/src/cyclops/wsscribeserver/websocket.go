package main

import (
	"time"

	"github.com/gorilla/websocket"
)

type MessageReader interface {
	ReadMessage() (msgType int, payload []byte, err error)
}

type MessageWriter interface {
	WriteMessage(msgType int, payload []byte) error
}

type Messager interface {
	MessageReader
	MessageWriter
	Close() error
}

type Socket struct {
	*websocket.Conn
}

func NewSocket(conn *websocket.Conn) *Socket {
	conn.SetReadDeadline(time.Now().Add(pongWait))
	conn.SetPongHandler(func(string) error { conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	return &Socket{conn}
}

func (s *Socket) WriteMessage(msgType int, payload []byte) error {
	s.Conn.SetWriteDeadline(time.Now().Add(writeWait))
	return s.Conn.WriteMessage(msgType, payload)
}
