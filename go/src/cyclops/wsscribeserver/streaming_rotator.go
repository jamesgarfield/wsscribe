package main

import (
	"context"
	"io"
	"sync"
	"time"

	speech "cloud.google.com/go/speech/apiv1"
	"github.com/pkg/errors"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
)

const streamingClientTimeout = 50

// StreamingRotator manages the cycling of StreamingRecognize clients
// Clients will be rotated under two conditions
// * After a "final" translation is received
// * After the streamingClientTimeout has been reached
//
// The initial state of the rotator is to have both the `send` and `recv` clients
// reference the same underlying client. During a rotation, the `send`` client is
// replaced with a new client, while the `recv` client is drained of any remaining
// responses. After `io.EOF` is received, the `recv` client will again point to the
// `send` client, resetting the state.
type StreamingRotator struct {
	Ctx      context.Context
	Client   *speech.Client
	send     *streamingClient
	sendLock *sync.RWMutex
	recv     *streamingClient
	recvLock *sync.RWMutex
	drain    bool
}

func NewStreamingRotator(ctx context.Context) (*StreamingRotator, error) {
	speechCli, err := speech.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	streamingCli, err := newStreamingClient(ctx, speechCli)
	if err != nil {
		return nil, err
	}
	return &StreamingRotator{
		Ctx:      ctx,
		Client:   speechCli,
		send:     streamingCli,
		recv:     streamingCli,
		sendLock: &sync.RWMutex{},
		recvLock: &sync.RWMutex{},
	}, nil
}

func (sr *StreamingRotator) Tick(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			logger.Info("closing streamer")
			return nil
		case <-sr.send.C:
			logger.Info("cycling send client")
			if err := sr.newSendClient(); err != nil {
				return err
			}
		default:
		}
	}
}

func (sr *StreamingRotator) Send(r *speechpb.StreamingRecognizeRequest) error {
	sr.sendLock.RLock()
	defer sr.sendLock.RUnlock()
	return errors.WithStack(sr.send.Send(r))
}

func (sr *StreamingRotator) Recv() (*speechpb.StreamingRecognizeResponse, error) {
	sr.recvLock.RLock()
	resp, err := sr.recv.Recv()
	sr.recvLock.RUnlock()

	switch err {
	case nil:
	case io.EOF:
		sr.swapAfterDrain()
	default:
		return nil, err
	}

	if resp.GetError() != nil {
		return resp, nil
	}

	results := resp.GetResults()
	if len(results) == 0 {
		return resp, nil
	}

	if results[0].IsFinal {
		if err := sr.newSendClient(); err != nil {
			return nil, err
		}
	}
	return resp, nil
}

func (sr *StreamingRotator) swapAfterDrain() {
	logger.Info("swapping")
	defer logger.Info("...end swapping")
	sr.sendLock.RLock()
	sr.recvLock.Lock()
	sr.recv = sr.send
	sr.drain = false
	sr.recvLock.Unlock()
	sr.sendLock.RUnlock()
}

func (sr *StreamingRotator) isDraining() bool {
	sr.recvLock.RLock()
	defer sr.recvLock.RUnlock()
	return sr.drain
}

func (sr *StreamingRotator) newSendClient() error {
	logger.Info("new sender...")
	defer logger.Info("...end new sender")

	// if we're in the `drain`` state we already have a new `send` client
	if sr.isDraining() {
		logger.Info("...still draining...")
		return nil
	}

	sr.recvLock.Lock()
	defer sr.recvLock.Unlock()
	sr.sendLock.Lock()
	defer sr.sendLock.Unlock()

	sr.send.CloseSend()

	cli, err := newStreamingClient(sr.Ctx, sr.Client)
	if err != nil {
		return err
	}
	sr.drain = true
	sr.send = cli
	return nil
}

type streamingClient struct {
	speechpb.Speech_StreamingRecognizeClient
	C <-chan time.Time
}

func newStreamingClient(ctx context.Context, c *speech.Client) (*streamingClient, error) {
	srCli, err := c.StreamingRecognize(ctx)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	config := &speechpb.StreamingRecognitionConfig{
		Config: &speechpb.RecognitionConfig{
			Encoding:        speechpb.RecognitionConfig_LINEAR16,
			SampleRateHertz: 44100,
			LanguageCode:    "en-US",
		},
		InterimResults: true,
	}

	req := &speechpb.StreamingRecognizeRequest{
		StreamingRequest: &speechpb.StreamingRecognizeRequest_StreamingConfig{
			StreamingConfig: config,
		},
	}

	if err := srCli.Send(req); err != nil {
		return nil, errors.WithStack(err)
	}
	return &streamingClient{srCli, time.After(time.Second * streamingClientTimeout)}, err
}
