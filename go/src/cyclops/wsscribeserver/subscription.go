package main

import (
	stderr "errors"
	"fmt"
	"time"

	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
)

// a subscription represents the pairing of a connection to a room
// it can be used to signal joining and leaving a room
type subscription struct {
	conn               *connection
	roomName, userName string
	audio              audioTranscriber
}

func (s subscription) String() string {
	return fmt.Sprintf("%s:%s", s.roomName, s.userName)
}

type audioSender interface {
	Send(*speechpb.StreamingRecognizeRequest) error
}
type audioReceiver interface {
	Recv() (*speechpb.StreamingRecognizeResponse, error)
}

type audioTranscriber interface {
	audioSender
	audioReceiver
}

var errUnexpectedMessageType = stderr.New("unexpected message type")

func (s subscription) readWS(h *hub) {
	defer func() { logger.Infof("%s parting from ws", s); h.part <- s }()
	for {
		err := readWS(s.conn, s.audio)
		switch errors.Cause(err) {
		case nil:
			return
		case errUnexpectedMessageType:
			logger.Errorf("[room=%s] %+v", s.roomName, err)
		default:
			logger.Errorf("[room=%s] %+v", s.roomName, err)
			return
		}
	}
}

func readWS(r MessageReader, at audioSender) error {
	for {
		msgType, msg, err := r.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				return err
			}
			return nil
		}
		switch msgType {
		case websocket.BinaryMessage:
			if err := at.Send(newAudioRequest(msg)); err != nil {
				return err
			}
		case websocket.TextMessage:
			return errors.Wrapf(errUnexpectedMessageType, "text: %s", msg)
		default:
			return errors.Wrapf(errUnexpectedMessageType, "type: %d", msgType)
		}
	}
}

func (s subscription) readTranscription(h *hub) {
	defer func() { logger.Infof("%s parting from transcription", s); h.part <- s }()
	for {
		result, err := readTranscription(s.audio)
		if err != nil {
			logger.Errorf("[room=%s] %+v", s.roomName, err)
			return
		}
		top := result.GetAlternatives()[0]
		msg := message{s.roomName, s.userName, []byte(top.GetTranscript()), result.IsFinal}
		if result.IsFinal {
			logger.Infof("%s final result %s", s, msg)
		}
		h.broadcast <- msg
	}
}

var errRPCStatus = stderr.New("rpc status error")

func readTranscription(ar audioReceiver) (*speechpb.StreamingRecognitionResult, error) {
	for {
		resp, err := ar.Recv()
		if err != nil {
			return nil, err
		}

		if errStatus := resp.GetError(); errStatus != nil {
			return nil, errors.Wrapf(errRPCStatus, "Code: %d; %s", errStatus.Code, errStatus.String())

		}
		results := resp.GetResults()
		if len(results) == 0 {
			continue
		}

		return results[0], nil
	}

}

func (s subscription) write(h *hub) {
	c := s.conn
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
	}()

	if err := write(c, c.send, ticker); err != nil {
		logger.Errorf("writing messages: %+v", err)
	}
}

func write(mr MessageWriter, msgChan <-chan []byte, ticker *time.Ticker) error {
	for {
		select {
		case message, ok := <-msgChan:
			if !ok {
				err := mr.WriteMessage(websocket.CloseMessage, []byte{})
				return err
			}
			if err := mr.WriteMessage(websocket.TextMessage, message); err != nil {
				return err
			}
		case <-ticker.C:
			if err := mr.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return err
			}
		}
	}
}

func newAudioRequest(aud []byte) *speechpb.StreamingRecognizeRequest {
	return &speechpb.StreamingRecognizeRequest{
		StreamingRequest: &speechpb.StreamingRecognizeRequest_AudioContent{
			AudioContent: aud,
		},
	}
}
