package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
)

// a room represents all connections currently in a room
type room map[*connection]struct{}

func (r *room) String() string {
	return fmt.Sprintf("room[%p](%d)", r, len(*r))
}

func (r room) add(c *connection) {
	r[c] = struct{}{}
}

func (r room) remove(c *connection) error {
	_, ok := r[c]
	if !ok {
		return nil
	}
	delete(r, c)
	return c.Close()
}

// sends a message to all connections in the room
// returns true if any messages were sent, or false if the room is empty
func (r room) broadcast(m message) (bool, error) {
	type packet struct {
		UserName  string    `json:"userName"`
		Data      string    `json:"data"`
		IsFinal   bool      `json:"isFinal"`
		Timestamp time.Time `json:"timestamp"`
	}
	b, err := json.Marshal(packet{m.userName, string(m.data), m.isFinal, time.Now()})
	if err != nil {
		return false, errors.WithStack(err)
	}
	for conn := range r {
		select {
		case conn.send <- b:
		default:
			err := r.remove(conn)
			if err != nil {
				logger.Errorf("closing connection: %s\n", err)
			}
		}
	}
	return len(r) > 0, nil
}
