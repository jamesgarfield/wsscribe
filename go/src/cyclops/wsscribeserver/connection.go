package main

// connection acts as the mediator to a websocket connection
// any bytes sent via the `send` channel will be transmitted
// back to the client
type connection struct {
	*Socket
	send chan []byte
}

func (c *connection) Close() error {
	close(c.send)
	return c.Socket.Close()
}
