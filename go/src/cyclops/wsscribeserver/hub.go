package main

import (
	stderr "errors"
	"fmt"

	"github.com/pkg/errors"
)

// message represents a packet of data to broadcast
// it will be broadcast to all users in roomName
// it represents data originating from userName
type message struct {
	roomName, userName string
	data               []byte
	isFinal            bool
}

func (m message) String() string {
	return fmt.Sprintf("%s:%s - %s", m.roomName, m.userName, m.data)
}

// a directory represents all rooms
type directory map[string]room

func (d directory) join(s subscription) {
	r := d[s.roomName]
	if r == nil {
		r = room{}
		d[s.roomName] = r
	}
	r.add(s.conn)
	logger.Infof("%s joined", s)
}

func (d directory) part(s subscription) {
	r := d[s.roomName]
	if r == nil {
		return
	}
	r.remove(s.conn)
	logger.Infof("%s parted", s)
}

var errInvalidRoom = stderr.New("invalid room")

func (d directory) broadcast(m message) error {
	r := d[m.roomName]
	if r == nil {
		return errors.Wrap(errInvalidRoom, m.roomName)
	}
	ok, err := r.broadcast(m)
	if err != nil {
		return err
	}
	if !ok { // no one left in room
		delete(d, m.roomName)
	}
	return nil
}

type hub struct {
	directory
	broadcast chan message
	join      chan subscription
	part      chan subscription
}

func (h *hub) run() {
	for {
		select {
		case sub := <-h.join:
			h.directory.join(sub)
		case sub := <-h.part:
			h.directory.part(sub)
		case msg := <-h.broadcast:
			err := h.directory.broadcast(msg)
			if err != nil {
				logger.Println(err)
			}
		}
	}
}
