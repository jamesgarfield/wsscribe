package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	speech "cloud.google.com/go/speech/apiv1"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
)

const (
	version = "0.0.40"

	envPrefix = "wsscribe"
	logPrefix = ""
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 1024
)

var (
	logger = logrus.New()
)

type envVars struct {
	Port int `default:"8080"`
}

func main() {

	env := envVars{}
	envconfig.MustProcess(envPrefix, &env)
	router := mux.NewRouter()
	h := &hub{
		broadcast: make(chan message),
		join:      make(chan subscription),
		part:      make(chan subscription),
		directory: directory{},
	}
	go h.run()
	handler := RoomHandler{h}
	router.Handle("/rooms/{room}/{user}", handler)
	router.Handle("/version", http.HandlerFunc(VersionHandler))
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", env.Port),
		Handler: router,
	}

	logger.Infof("starting http server (%s) on port %d", version, env.Port)
	logger.Fatal(server.ListenAndServe())
}

type RoomHandler struct {
	*hub
}

func (h RoomHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	roomName := vars["room"]
	userName := vars["user"]

	logger.Infof("%s:%s connecting", roomName, userName)
	audioCli, err := NewStreamingRotator(r.Context())
	if err != nil {
		logger.Errorf("cloud not start transcription: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(nil)
		return
	}
	ctx, cancel := context.WithCancel(r.Context())
	defer cancel()
	go audioCli.Tick(ctx)

	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     func(*http.Request) bool { return true },
	}
	wsconn, err := upgrader.Upgrade(w, r, http.Header{})
	if err != nil {
		logger.Errorf("could not upgrade to websockets: %s", err)
		return
	}
	conn := &connection{NewSocket(wsconn), make(chan []byte, 512)}

	sub := subscription{conn, roomName, userName, audioCli}

	h.hub.join <- sub
	go sub.write(h.hub)
	go sub.readTranscription(h.hub)
	sub.readWS(h.hub)
	logger.Infof("%s ended", sub)

}

func NewStreamingRecognize(ctx context.Context) (speechpb.Speech_StreamingRecognizeClient, error) {
	speechCli, err := speech.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	srCli, err := speechCli.StreamingRecognize(ctx)
	if err != nil {
		return nil, err
	}
	config := &speechpb.StreamingRecognitionConfig{
		Config: &speechpb.RecognitionConfig{
			Encoding:        speechpb.RecognitionConfig_LINEAR16,
			SampleRateHertz: 44100,
			LanguageCode:    "en-US",
		},
		InterimResults: true,
	}

	req := &speechpb.StreamingRecognizeRequest{
		StreamingRequest: &speechpb.StreamingRecognizeRequest_StreamingConfig{
			StreamingConfig: config,
		},
	}

	err = srCli.Send(req)
	return srCli, err
}

func VersionHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("version:%s", version)))
}
