package main

import (
	"fmt"
	"log"
)

type Logger struct {
	*log.Logger
}

func (l Logger) Errorf(format string, v ...interface{}) {
	l.Printf("ERROR: %s\n", fmt.Sprintf(format, v...))
}

func (l Logger) Infof(format string, v ...interface{}) {
	l.Printf("INFO: %s\n", fmt.Sprintf(format, v...))
}
